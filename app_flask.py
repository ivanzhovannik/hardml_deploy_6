import flask
from flask import jsonify
import os

app = flask.Flask(__name__)

SECRET_NUMBER = os.environ['SECRET_NUMBER']


@app.route('/return_secret_number', methods=['GET'])
def return_secret_number():
    return jsonify({'secret_number': SECRET_NUMBER})


if __name__ == "__main__":
    app.run()
else:
    application = app
