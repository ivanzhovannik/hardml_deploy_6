FROM python:3
ARG SECRET_NUMBER_ARG

COPY ./requirements.txt /app/requirements.txt

WORKDIR /app

RUN pip install -r requirements.txt

COPY . /app

EXPOSE 5011
ENV export FLASK_APP main
ENV SECRET_NUMBER ${SECRET_NUMBER_ARG}

ENTRYPOINT [ "/bin/bash" ]
CMD [ "start.sh" ]